// translate
chrome.runtime.onInstalled.addListener(() => {
	chrome.contextMenus.create({
		id: "translate",
		title: "translate",
        contexts: ["selection"]
	});
});

chrome.runtime.onInstalled.addListener(() => {
	chrome.contextMenus.create({
        id: "google-translate",
        type: "checkbox",
        parentId: "translate",
        title: "Google Translate",
        contexts: ["selection", "link"]
	});
});

chrome.runtime.onInstalled.addListener(() => {
	chrome.contextMenus.create({
        id: "baidu-translate",
        type: "checkbox",
        parentId: "translate",
        title: "Baidu Translate",
        contexts: ["selection", "link"]
    });
});

// Listen for click events
let openTabId = 0;

function toTranslate (url) {
    if (openTabId > 0) {
        chrome.tabs.get(openTabId, function(t) {
            if (t) {
                chrome.windows.update(t.windowId, {focused: true}); // 更新窗口
                chrome.tabs.update(openTabId, {active: true, url});  // 更新tab
            } else {
                chrome.tabs.create({
                    url,
                }, function(tab) {
                    openTabId = tab.id;
                })
            }
        });
    } else {
        chrome.tabs.create({
            url,
        }, function (t) {
            console.log("else t", t);
            openTabId = t.id;
        });
    }
}

chrome.contextMenus.onClicked.addListener(function(info) {
    if (info.menuItemId === "baidu-translate") {
        toTranslate(`https://fanyi.baidu.com/#en/zh/${info.selectionText}`)
    } else if (info.menuItemId === "google-translate") {
        toTranslate(`https://translate.google.com/?sl=en&tl=zh-CN&text=${info.selectionText}&op=translate`)
    }
});